# TDD Workshop

This is the starting point of a TDD workshop where we test drive a circular buffer.

Clone the repository, make sure you have GCC and Python installed.

Open the repository in vscode and checkout the first commit.

In a terminal, run `pip install -r requirements.txt`

# Inspiration

This is heavily inspired by [James Grennings' article](http://blog.wingman-sw.com/tdd-guided-by-zombies).
