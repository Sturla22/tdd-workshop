#include "CppUTest/TestHarness.h"

extern "C"
{
#include "CircularBuffer.h"
}

#define CAPACITY 10
#define DEFAULT_VALUE 0

TEST_GROUP(CircularBuffer)
{
    CircularBuffer *buffer;

    void setup()
    {
        buffer = CircularBuffer_Create(CAPACITY, DEFAULT_VALUE);
    }

    void teardown()
    {
        CircularBuffer_Destroy(buffer);
    }
};

TEST(CircularBuffer, can_be_created_and_destroyed)
{
}

/*
TDD guided by ZOMBIES
Z – Zero
O – One
M – Many (or More complex)

B – Boundary Behaviors
I – Interface definition
E – Exercise Exceptional behavior

S – Simple Scenarios, Simple Solutions

Basic interface:
 - `Put()` a new integer in
 - `Get()` the oldest out
 - If it `IsFull()` it will reject new attempts to put
 - If it `IsEmpty()`, a get returns a default value
 - You can specify the default value during `Create()`

Scenarios to test:
 - Empty after creation
 - Not Full after creation
 - Not empty after put
 - Empty after put then get
 - put equals get for one
 - Happy path – FIFO
 - adjustable capacity
 - Underflow
 - Overflow
 - Full at overflow
 - Wrap around
 - Full after wrapping
 - put to full fails
 - get from empty returns default value
 - put to full does not damage contents
 - get from empty does no harm
*/

// Zero
TEST(CircularBuffer, is_empty_after_creation)
{
    CHECK_TRUE(CircularBuffer_IsEmpty(buffer));
}

TEST(CircularBuffer, is_not_full_after_creation)
{
    CHECK_FALSE(CircularBuffer_IsFull(buffer));
}

// One
TEST(CircularBuffer, is_not_empty_after_put)
{
    CircularBuffer_Put(buffer, 42);
    CHECK_FALSE(CircularBuffer_IsEmpty(buffer));
}

TEST(CircularBuffer, is_empty_after_put_then_get)
{
    CircularBuffer_Put(buffer, 42);
    CircularBuffer_Get(buffer);
    CHECK_TRUE(CircularBuffer_IsEmpty(buffer));
}

TEST(CircularBuffer, get_equals_put_for_one_item)
{
    CircularBuffer_Put(buffer, 42);
    LONGS_EQUAL(42, CircularBuffer_Get(buffer));
}

// Many
TEST(CircularBuffer, put_get_is_fifo)
{
    CircularBuffer_Put(buffer, 41);
    CircularBuffer_Put(buffer, 42);
    CircularBuffer_Put(buffer, 43);
    LONGS_EQUAL(41, CircularBuffer_Get(buffer));
    LONGS_EQUAL(42, CircularBuffer_Get(buffer));
    LONGS_EQUAL(43, CircularBuffer_Get(buffer));
}

TEST(CircularBuffer, report_capacity)
{
    LONGS_EQUAL(CAPACITY, CircularBuffer_Capacity(buffer));
}

TEST(CircularBuffer, capacity_is_adjustable)
{
    CircularBuffer * buffer = CircularBuffer_Create(CAPACITY+2, DEFAULT_VALUE);
    LONGS_EQUAL(CAPACITY+2, CircularBuffer_Capacity(buffer));
    CircularBuffer_Destroy(buffer);
}

static void fillItUp(CircularBuffer * buffer)
{
    for (int i = 0; i < CircularBuffer_Capacity(buffer); i++)
        CircularBuffer_Put(buffer, i);
}
 
TEST(CircularBuffer, fill_to_capacity)
{
    fillItUp(buffer);
    CHECK_TRUE(CircularBuffer_IsFull(buffer));
}

TEST(CircularBuffer, is_not_full_after_get_from_full_buffer)
{
    fillItUp(buffer);
    CircularBuffer_Get(buffer);
    CHECK_FALSE(CircularBuffer_IsFull(buffer));
}

TEST(CircularBuffer, force_a_buffer_wraparound)
{
    CircularBuffer * buffer = CircularBuffer_Create(2, DEFAULT_VALUE);
    CircularBuffer_Put(buffer, 1);
    CircularBuffer_Put(buffer, 2);
    CircularBuffer_Get(buffer);
    CircularBuffer_Put(buffer, 3);
    LONGS_EQUAL(2, CircularBuffer_Get(buffer));
    LONGS_EQUAL(3, CircularBuffer_Get(buffer));
    CHECK_TRUE(CircularBuffer_IsEmpty(buffer));
    CircularBuffer_Destroy(buffer);
}

TEST(CircularBuffer, full_after_wrapping)
{
    CircularBuffer * buffer = CircularBuffer_Create(2, DEFAULT_VALUE);
    CircularBuffer_Put(buffer, 1);
    CircularBuffer_Put(buffer, 2);
    CircularBuffer_Get(buffer);
    CircularBuffer_Put(buffer, 3);
    CHECK_TRUE(CircularBuffer_IsFull(buffer));
    CircularBuffer_Destroy(buffer);
}

// Exercise Exceptional Behavior
TEST(CircularBuffer, put_to_full_fails)
{
    CircularBuffer * buffer = CircularBuffer_Create(1, DEFAULT_VALUE);
    CHECK_TRUE(CircularBuffer_Put(buffer, 1));
    CHECK_FALSE(CircularBuffer_Put(buffer, 2));
    CircularBuffer_Destroy(buffer);
}

TEST(CircularBuffer, get_from_empty_returns_default_value)
{
    LONGS_EQUAL(DEFAULT_VALUE, CircularBuffer_Get(buffer));
}

TEST(CircularBuffer, put_to_full_does_not_damage_contents)
{
    CircularBuffer * buffer = CircularBuffer_Create(1, DEFAULT_VALUE);
    CircularBuffer_Put(buffer, 1);
    CHECK_FALSE(CircularBuffer_Put(buffer, 2));
    LONGS_EQUAL(1, CircularBuffer_Get(buffer));
    CHECK_TRUE(CircularBuffer_IsEmpty(buffer));
    CircularBuffer_Destroy(buffer);
}

TEST(CircularBuffer, get_from_empty_does_no_harm)
{
    CircularBuffer * buffer = CircularBuffer_Create(1, DEFAULT_VALUE);
    CircularBuffer_Get(buffer);
    CHECK_TRUE(CircularBuffer_IsEmpty(buffer));
    CHECK_FALSE(CircularBuffer_IsFull(buffer));
    CircularBuffer_Put(buffer, 1);
    CHECK_TRUE(CircularBuffer_IsFull(buffer));
    CHECK_FALSE(CircularBuffer_IsEmpty(buffer));
    LONGS_EQUAL(1, CircularBuffer_Get(buffer));
    CHECK_TRUE(CircularBuffer_IsEmpty(buffer));
    CircularBuffer_Destroy(buffer);
}
