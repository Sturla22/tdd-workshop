#pragma once
#include <stdbool.h>

typedef struct CircularBufferStruct CircularBuffer;
 
CircularBuffer * CircularBuffer_Create(int capacity, int defaultValue);
void CircularBuffer_Destroy(CircularBuffer *);
bool CircularBuffer_IsEmpty(CircularBuffer *);
bool CircularBuffer_IsFull(CircularBuffer *);
bool CircularBuffer_Put(CircularBuffer *, int);
int CircularBuffer_Get(CircularBuffer *);
int CircularBuffer_Capacity(CircularBuffer *);
